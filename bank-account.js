const depositInput = document.getElementById("deposit-input");
const withdrawInput = document.getElementById("withdraw-input");
const depositBtn = document.getElementById("deposit-btn");
const withdrawBtn = document.getElementById("withdraw-btn");

depositBtn.addEventListener("click", () => {
  const value = depositInput.value;
  // console.log("clicked");
  const depositValue = Number(deposit.innerText) + Number(value);
  deposit.innerText = depositValue;
  const balanceValue = Number(balance.innerText) + Number(value);
  balance.innerText = balanceValue;
  depositInput.value = "";
});

withdrawBtn.addEventListener("click", () => {
  const value = withdrawInput.value;
  if (Number(value) === 0) {
    alert("You don't have any balance to withdraw");
  } else if (Number(value) > Number(balance.innerText)) {
    alert("You don't have that much balance to withdraw");
  } else {
    const balanceValue = Number(balance.innerText) - Number(value);
    const withdrawValue = Number(withdraw.innerText) + Number(value);
    withdraw.innerText = withdrawValue;
    balance.innerText = balanceValue;
    withdrawInput.value = "";
  }
});
